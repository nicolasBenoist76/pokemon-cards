const cardContainer = document.getElementById("card-container");
const paginationContainer = document.getElementById("pagination");
const filterInput = document.getElementById("filter");
const loaderElement = document.getElementById("loader");
const apiURL = "https://api.pokemontcg.io/v2/cards";
const pageSize = 24;
let currentPage = 1;
let searchTerm = ""; // le terme de recherche courant
let isLoading = false;
let cachedData = {}; // déclarez un objet pour stocker les données mises en cache

// récupère les données depuis l'API
async function fetchCards() {
  const response = await fetch(apiURL);
  const responseData = await response.json();
  return responseData?.data || [];
}

// filtre les données en fonction du terme de recherche
function filterCards(cachedData, searchTerm) {
  if (searchTerm === "") {
    return cachedData;
  }
  return cachedData.filter(
    card => card.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
            card.types.some(type => type.toLowerCase().includes(searchTerm.toLowerCase()))
  );
}

async function fetchAndRenderCards() {
  try {
    if (isLoading) {
      return; // ne commencez pas une nouvelle requête si une est déjà en cours
    }
    isLoading = true;
    loaderElement.style.display = "block";
    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    let filteredData = [];

    if (searchTerm !== "") {
      // vérifiez d'abord si les données sont dans le cache
      if (cachedData.hasOwnProperty(searchTerm)) {
        filteredData = cachedData[searchTerm];
      } else {
        // récupérer les données depuis l'API et les mettre en cache
        const cards = await fetchCards();
        filteredData = filterCards(cards, searchTerm);
        cachedData[searchTerm] = filteredData;
      }
    } else {
      // vérifiez d'abord si les données sont dans le cache
      if (cachedData.hasOwnProperty("all")) {
        filteredData = cachedData["all"];
      } else {
        // récupérer les données depuis l'API et les mettre en cache
        const cards = await fetchCards();
        filteredData = cards;
        cachedData["all"] = filteredData;
      }
    }

    const paginatedData = filteredData.slice(startIndex, endIndex);
    renderCards(paginatedData);
    renderPagination(filteredData.length);
  } catch (error) {
    console.error(error);
  } finally {
    isLoading = false;
    loaderElement.style.display = "none";
  }
}


function renderCards(cards) {
  cardContainer.innerHTML = "";
  cards.forEach((card) => {
    const cardDiv = document.createElement("div");
    cardDiv.className = "card";
    const cardImageDiv = document.createElement("div");
    cardImageDiv.className = "card-image";
    cardImageDiv.style.backgroundImage = `url(${card.images.small})`;
    const popover = document.createElement("div");
    popover.className = "popover";
    popover.innerHTML = `
      <div class="popover-data">
        <span>+</span>
        <img src="${card.images.small}" width="50" class="popover-infos">
        <span class="popover-infos">Name: ${card.name}</span>
        <span class="popover-infos">HP: ${card.hp}</span>
        <span class="popover-infos">Level: ${card.level}</span>
        <span class="popover-infos">Type: ${card.types}</span>
        <span class="popover-infos">Evolution: ${card.evolvesFrom}</span>
      </div>
    `
    cardImageDiv.appendChild(popover);
    cardDiv.appendChild(cardImageDiv);
    cardContainer.appendChild(cardDiv);
  });
}

function renderPagination(totalCards) {
  paginationContainer.innerHTML = "";

  // Calculer le nombre total de pages
  const totalPages = Math.ceil(totalCards / pageSize);

  // Limiter le nombre de boutons à afficher
  const maxButtonsToShow = 5;
  let startPage = Math.max(1, currentPage - Math.floor(maxButtonsToShow / 2));
  let endPage = Math.min(startPage + maxButtonsToShow - 1, totalPages);

  for (let i = startPage; i <= endPage; i++) {
    const button = document.createElement("button");
    button.className = "pagination-button";
    button.textContent = i;
    if (i === currentPage) {
      button.classList.add("active");
      scrollTo(0, 0);
    }
    button.addEventListener("click", handlePaginationButtonClick);
    paginationContainer.appendChild(button);
  }
}

function handlePaginationButtonClick() {
  currentPage = parseInt(this.textContent);
  fetchAndRenderCards();
}

async function handleFilterInput() {
  searchTerm = filterInput.value.trim();
  currentPage = 1; // réinitialiser la page courante
  fetchAndRenderCards();
}

filterInput.addEventListener("input", handleFilterInput);

fetchAndRenderCards();

